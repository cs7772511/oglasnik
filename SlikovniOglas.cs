﻿namespace OglasnaPloca
{
    internal class SlikovniOglas : Oglas
    {
        private string slikaUrl;
        public SlikovniOglas(string naslov, string opis, string autor, DateTime datum, string slikaUrl) : base(autor, naslov, opis, datum)
        {
            SlikaUrl=slikaUrl;
        }
        public string SlikaUrl
        {
            get { return slikaUrl; }
            set
            {
                if (value.EndsWith(".png")) { slikaUrl = value; }
                else
                {
                    throw new ArgumentException("Link slike mora završavati sa \".png\"!");
                }
            }
        }
        public override string ToString()
        {
            return $"{Naslov}, {Opis}, {Autor} ({Datum}) {Environment.NewLine}Slika: {SlikaUrl}";
        }
    }
}
