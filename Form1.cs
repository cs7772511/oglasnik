namespace OglasnaPloca
{
    public partial class Form1 : Form
    {
        Oglasi oglasi;
        public Form1()
        {
            InitializeComponent();
            oglasi = new Oglasi();
        }
        private void RefreshDisplay()
        {
            tbOglasi.Clear();
            foreach (var oglas in oglasi.PrikaziOglase())
            {
                tbOglasi.AppendText(oglas.ToString() + Environment.NewLine);
            }
        }
        private void btnDodajOglas_Click(object sender, EventArgs e)
        {
            if (cbSlikovniOglas.Checked == true)
            {
                oglasi.DodajOglas(new SlikovniOglas(tbNaslov.Text, tbOpis.Text, "Korisnik", DateTime.Now, tbSlikaUrl.Text));
                RefreshDisplay();
            }
            else if (cbVideoOglas.Checked == true)
            {
                oglasi.DodajOglas(new VideoOglas(tbNaslov.Text, tbOpis.Text, "Korisnik", DateTime.Now, tbVideoUrl.Text));
                RefreshDisplay();
            }
            else if (cbVideoOglas.Checked == true && cbSlikovniOglas.Checked == true)
            {
                throw new Exception("Oglas ne mo�e biti i slikovni i video.");
            }
            else
            {
                oglasi.DodajOglas(new Oglas(tbNaslov.Text, tbOpis.Text, "Korisnik", DateTime.Now));
                RefreshDisplay();
            }
        }

        private void btnOglasiClear_Click(object sender, EventArgs e)
        {
            oglasi.Clear();
            tbOglasi.Clear();
        }
    }
}