﻿namespace OglasnaPloca
{
    internal class Oglas
    {
        public Oglas(string naslov, string opis, string autor, DateTime datum)
        {
            Naslov = naslov;
            Opis = opis;
            Autor = autor;
            Datum = datum;
        }
        public string Naslov { get; set; }
        public string Opis { get; set; }
        public string Autor { get; set; }
        public DateTime Datum { get; set; }
        public override string ToString()
        {
            return $"{Naslov}, {Opis}, {Autor} ({Datum}) {Environment.NewLine}(nema slike/video)";
        }
    }
}
