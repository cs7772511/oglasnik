﻿namespace OglasnaPloca
{
    internal class Oglasi
    {
        private List<Oglas> oglasi;
        public Oglasi()
        {
            oglasi = new List<Oglas>();
        }
        public void DodajOglas(Oglas oglas)
        {
            oglasi.Add(oglas);
        }
        public List<Oglas> PrikaziOglase()
        {
            return oglasi;
        }
        public void Clear()
        {
            oglasi.Clear();
        }
    }
}
