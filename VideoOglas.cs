﻿namespace OglasnaPloca
{
    internal class VideoOglas : Oglas
    {
        private string videoUrl;
        public VideoOglas(string naslov, string opis, string autor, DateTime datum, string videoUrl) : base(autor, naslov, opis, datum)
        {
            VideoUrl = videoUrl;
        }
        public string VideoUrl
        {
            get { return videoUrl; }
            set
            {
                if (value.EndsWith(".mp3")) { videoUrl = value; }
                else
                {
                    throw new ArgumentException("LVideo link mora završavati sa \".mp3\"!");
                }
            }
        }
        public override string ToString()
        {
            return $"{Naslov}, {Opis}, {Autor} ({Datum}) {Environment.NewLine}Video: {VideoUrl}";
        }
    }
}
