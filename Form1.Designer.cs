﻿namespace OglasnaPloca
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tbNaslov=new TextBox();
            lblNaslov=new Label();
            cbSlikovniOglas=new CheckBox();
            btnDodajOglas=new Button();
            tbOpis=new TextBox();
            tbSlikaUrl=new TextBox();
            tbVideoUrl=new TextBox();
            cbVideoOglas=new CheckBox();
            lblOpis=new Label();
            tbOglasi=new TextBox();
            lblOglasi=new Label();
            btnOglasiClear=new Button();
            SuspendLayout();
            // 
            // tbNaslov
            // 
            tbNaslov.Location=new Point(36, 36);
            tbNaslov.Name="tbNaslov";
            tbNaslov.Size=new Size(218, 23);
            tbNaslov.TabIndex=0;
            // 
            // lblNaslov
            // 
            lblNaslov.AutoSize=true;
            lblNaslov.Location=new Point(36, 9);
            lblNaslov.Name="lblNaslov";
            lblNaslov.Size=new Size(80, 15);
            lblNaslov.TabIndex=1;
            lblNaslov.Text="Naslov oglasa";
            // 
            // cbSlikovniOglas
            // 
            cbSlikovniOglas.AutoSize=true;
            cbSlikovniOglas.Location=new Point(36, 241);
            cbSlikovniOglas.Name="cbSlikovniOglas";
            cbSlikovniOglas.Size=new Size(98, 19);
            cbSlikovniOglas.TabIndex=2;
            cbSlikovniOglas.Text="Slikovni oglas";
            cbSlikovniOglas.UseVisualStyleBackColor=true;
            // 
            // btnDodajOglas
            // 
            btnDodajOglas.Location=new Point(36, 385);
            btnDodajOglas.Name="btnDodajOglas";
            btnDodajOglas.Size=new Size(218, 40);
            btnDodajOglas.TabIndex=3;
            btnDodajOglas.Text="Dodaj oglas";
            btnDodajOglas.UseVisualStyleBackColor=true;
            btnDodajOglas.Click+=btnDodajOglas_Click;
            // 
            // tbOpis
            // 
            tbOpis.Location=new Point(36, 105);
            tbOpis.Multiline=true;
            tbOpis.Name="tbOpis";
            tbOpis.Size=new Size(218, 111);
            tbOpis.TabIndex=4;
            // 
            // tbSlikaUrl
            // 
            tbSlikaUrl.Location=new Point(36, 266);
            tbSlikaUrl.Name="tbSlikaUrl";
            tbSlikaUrl.Size=new Size(218, 23);
            tbSlikaUrl.TabIndex=5;
            // 
            // tbVideoUrl
            // 
            tbVideoUrl.Location=new Point(36, 329);
            tbVideoUrl.Name="tbVideoUrl";
            tbVideoUrl.Size=new Size(218, 23);
            tbVideoUrl.TabIndex=7;
            // 
            // cbVideoOglas
            // 
            cbVideoOglas.AutoSize=true;
            cbVideoOglas.Location=new Point(36, 304);
            cbVideoOglas.Name="cbVideoOglas";
            cbVideoOglas.Size=new Size(87, 19);
            cbVideoOglas.TabIndex=6;
            cbVideoOglas.Text="Video oglas";
            cbVideoOglas.UseVisualStyleBackColor=true;
            // 
            // lblOpis
            // 
            lblOpis.AutoSize=true;
            lblOpis.Location=new Point(36, 78);
            lblOpis.Name="lblOpis";
            lblOpis.Size=new Size(68, 15);
            lblOpis.TabIndex=8;
            lblOpis.Text="Opis oglasa";
            // 
            // tbOglasi
            // 
            tbOglasi.Location=new Point(357, 36);
            tbOglasi.Multiline=true;
            tbOglasi.Name="tbOglasi";
            tbOglasi.Size=new Size(292, 389);
            tbOglasi.TabIndex=9;
            // 
            // lblOglasi
            // 
            lblOglasi.AutoSize=true;
            lblOglasi.Location=new Point(357, 9);
            lblOglasi.Name="lblOglasi";
            lblOglasi.Size=new Size(43, 15);
            lblOglasi.TabIndex=10;
            lblOglasi.Text="Oglasi:";
            // 
            // btnOglasiClear
            // 
            btnOglasiClear.Location=new Point(655, 36);
            btnOglasiClear.Name="btnOglasiClear";
            btnOglasiClear.Size=new Size(24, 23);
            btnOglasiClear.TabIndex=11;
            btnOglasiClear.Text="C";
            btnOglasiClear.UseVisualStyleBackColor=true;
            btnOglasiClear.Click+=btnOglasiClear_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions=new SizeF(7F, 15F);
            AutoScaleMode=AutoScaleMode.Font;
            ClientSize=new Size(721, 450);
            Controls.Add(btnOglasiClear);
            Controls.Add(lblOglasi);
            Controls.Add(tbOglasi);
            Controls.Add(lblOpis);
            Controls.Add(tbVideoUrl);
            Controls.Add(cbVideoOglas);
            Controls.Add(tbSlikaUrl);
            Controls.Add(tbOpis);
            Controls.Add(btnDodajOglas);
            Controls.Add(cbSlikovniOglas);
            Controls.Add(lblNaslov);
            Controls.Add(tbNaslov);
            Name="Form1";
            Text="Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbNaslov;
        private Label lblNaslov;
        private CheckBox cbSlikovniOglas;
        private Button btnDodajOglas;
        private TextBox tbOpis;
        private TextBox tbSlikaUrl;
        private TextBox tbVideoUrl;
        private CheckBox cbVideoOglas;
        private Label lblOpis;
        private TextBox tbOglasi;
        private Label lblOglasi;
        private Button btnOglasiClear;
    }
}